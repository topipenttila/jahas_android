package com.example.topipenttila.jahas;

/**
 * Created by topipenttila on 06/10/16.
 *
 * A class for observing events that trigger actions
 */


public class MessageEvent {

    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}
