package com.example.topipenttila.jahas;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Juhani on 6.10.2016.
 */


public abstract class SendRequest extends AsyncTask<String, Void, String> {
    TextView requestTextView;
    String requestURL;
    String requestType;
    JSONObject requestParameters;
    private AccountFragment.FragmentCallback mFragmentCallback;
    /*
    public SendRequest(AccountFragment.FragmentCallback fragmentCallback) {
        mFragmentCallback = fragmentCallback;
    }
    */


    public SendRequest(AccountFragment.FragmentCallback fragmentCallback, String requestURL, String requestType, JSONObject requestParameters) {
        mFragmentCallback = fragmentCallback;
        this.requestURL = requestURL;
        this.requestType = requestType;
        this.requestParameters = requestParameters;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            URL url = new URL(requestURL);

            JSONObject dataParams = new JSONObject();
            dataParams = requestParameters;
            Log.e("dataParams: ", dataParams.toString());


            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000); // 15 seconds timeout
            conn.setConnectTimeout(15000);
            conn.setRequestMethod(requestType); //requestType
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getRequestDataString(dataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader((conn.getInputStream())));

                StringBuffer sb = new StringBuffer("");
                String line = "";

                while ((line = in.readLine()) != null) {
                    sb.append(line);
                    break;
                }

                in.close();
                return sb.toString();
            } else {
                return new String("REQUEST false: " + responseCode);
            }


        } catch (Exception e) {

            return "Failed";
            //return new String("REQUEST Exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String result) {
        Log.e("onPostExecute: ", result);
        //requestTextView.setText("requestTextView:" + result);
        mFragmentCallback.onTaskDone(result);
    }


    public String getRequestDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);

            if (first) {
                first = false;
            } else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return result.toString();
    }

    public abstract void onTaskDone(String result);
}