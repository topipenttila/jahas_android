package com.example.topipenttila.jahas;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

/**
 * Created by topipenttila on 04/10/16.
 */

public class SensorRestarterBroadcastReceiver extends WakefulBroadcastReceiver {
    private StepCounterService mSensorService;

    //Restarts the StepCounterService as Wakeful Service

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(SensorRestarterBroadcastReceiver.class.getSimpleName(), "Service Stops!");

        mSensorService = new StepCounterService(context);
        startWakefulService(context, new Intent(context, StepCounterService.class));
        System.out.println("ondes");

    }
}
