package com.example.topipenttila.jahas;

import android.app.Fragment;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Switch;

import com.mbientlab.metawear.AsyncOperation;
import com.mbientlab.metawear.Message;
import com.mbientlab.metawear.MetaWearBleService;
import com.mbientlab.metawear.MetaWearBoard;
import com.mbientlab.metawear.RouteManager;
import com.mbientlab.metawear.UnsupportedModuleException;
import com.mbientlab.metawear.module.MultiChannelTemperature;
import com.mbientlab.metawear.module.Temperature;
import com.mbientlab.metawear.module.Timer;
import com.mbientlab.metawear.processor.Maths;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.List;

import static com.example.topipenttila.jahas.R.id.stepCount;
import static com.google.android.gms.analytics.internal.zzy.c;
import static com.google.android.gms.analytics.internal.zzy.e;
import static com.google.android.gms.analytics.internal.zzy.m;
import static com.google.android.gms.analytics.internal.zzy.t;
import static com.mbientlab.metawear.MetaWearBoard.ConnectionStateHandler;
import static com.mbientlab.metawear.AsyncOperation.CompletionHandler;

/**
 * Created by topipenttila on 05/10/16.
 */

public class StepCounterFragment extends Fragment implements ServiceConnection {

    private int stepImageSwitch = 0;

    private TextView stepCountTextView, requestTextView, temperatureTextView, currentTempTextView;
    private Button submitStepsButton;

    private EditText macAddressBar;
    private ImageButton saveMacProperties, editMacProperties;

    private SharedPreferences sharedPreferences;
    private View rootView;
    private String stepsSinceLastCommit;
    private SharedPreferences.Editor editor;
    private ImageView stepImageView;
    private MetaWearBleService.LocalBinder serviceBinder;
    private ConnectionStateHandler stateHandler;

    //The MAC address of the MetaWear sensor board
    private String MW_MAC_ADDRESS;
    private MetaWearBoard mwBoard;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        EventBus.getDefault().register(this);

        rootView = inflater.inflate(R.layout.fragment_step_counter, container, false);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        /**
         * IMPORTANT
         * */
        MW_MAC_ADDRESS = getActivity().getResources().getString(R.string.sensor_mac_address);

        stepCountTextView = (TextView)rootView.findViewById(stepCount);
        submitStepsButton = (Button) rootView.findViewById(R.id.submitStepsButton);
        stepImageView = (ImageView)rootView.findViewById(R.id.stepImage);
        temperatureTextView = (TextView)rootView.findViewById(R.id.temperatureView);
        currentTempTextView = (TextView)rootView.findViewById(R.id.currentTemp);

        macAddressBar = (EditText) rootView.findViewById(R.id.macAddressBar);
        saveMacProperties  = (ImageButton) rootView.findViewById(R.id.saveMacProperties);
        editMacProperties = (ImageButton) rootView.findViewById(R.id.editMacProperties);
        macAddressBar.setText(sharedPreferences.getString("macAddress", getActivity().getResources().getString(R.string.sensor_mac_address)));
        macAddressBar.setEnabled(false);

        editMacProperties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveMacProperties.setVisibility(View.VISIBLE);
                macAddressBar.setEnabled(true);
                editMacProperties.setVisibility(View.INVISIBLE);
            }
        });

        saveMacProperties.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMacProperties.setVisibility(View.VISIBLE);
                macAddressBar.setEnabled(false);
                saveMacProperties.setVisibility(View.INVISIBLE);
                sharedPreferences.edit().putString("macAddress", String.valueOf(macAddressBar.getText())).apply();
                MW_MAC_ADDRESS = sharedPreferences.getString("macAddress" , getActivity().getResources().getString(R.string.sensor_mac_address));
                Log.e("macAddress", MW_MAC_ADDRESS);
            }
        });

        stepsSinceLastCommit = Integer.toString(sharedPreferences.getInt("stepsSinceLastCommit", 0));
        stepCountTextView.setText(stepsSinceLastCommit);
        editor = sharedPreferences.edit();
        // Bind the service when the activity is created
        getActivity().bindService(new Intent(getActivity(), MetaWearBleService.class),
                this, Context.BIND_AUTO_CREATE);

        stateHandler= new ConnectionStateHandler() {
            @Override
            public void connected() {
                Log.i("MainActivity", "Connected");
            }

            @Override
            public void disconnected() {
                Log.i("MainActivity", "Connected Lost");
            }

            @Override
            public void failure(int status, Throwable error) {
                Log.e("MainActivity", "Error connecting", error);
            }
        };



        submitStepsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject requestParameters = new JSONObject();

                try{
                    requestParameters.put("groupSteps", stepsSinceLastCommit);

                }catch(Exception e){
                }

                Log.e("requestParameters", String.valueOf(requestParameters));

                SendRequest updateGroupSteps = new SendRequest(new AccountFragment.FragmentCallback() {

                    @Override
                    public void onTaskDone(String result) {
                        if (result.equals("Failed")) {
                            Log.e("onTaskDone! ", result);
                            Toast toast = Toast.makeText(getActivity(), "Couldn't submit steps", Toast.LENGTH_SHORT);
                            toast.show();
                        } else {
                            //Reset the step counter values
                            editor.putInt("startingStepValue", 0);
                            editor.putInt("savedStepValue", 0);
                            editor.putInt("stepsSinceLastCommit", 0);
                            editor.putBoolean("firstTimeSensorChanged", true);
                            editor.apply();
                            stepCountTextView.setText("0");

                            Toast toast = Toast.makeText(getActivity(), "Steps committed", Toast.LENGTH_SHORT);
                            toast.show();

                        }
                        try{

                        }catch(Exception e){

                        }

                    }
                }, sharedPreferences.getString("ip" , "no ip") + "groups/"+sharedPreferences.getString("groupId", "")+"/updateGroupsSteps", "PUT", requestParameters){

                    @Override
                    public void onTaskDone(String result) {}
                };

                updateGroupSteps.execute();
            }
        });


        return rootView;
    }

    public void retrieveBoard() {
        final BluetoothManager btManager=
                (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        final BluetoothDevice remoteDevice=
                btManager.getAdapter().getRemoteDevice(MW_MAC_ADDRESS);

        // Create a MetaWear board object for the Bluetooth Device
        mwBoard= serviceBinder.getMetaWearBoard(remoteDevice);
    }

    public void connectBoard() {
        mwBoard.setConnectionStateHandler(stateHandler);
        mwBoard.connect();
    }

    public void updateRssi() {
        AsyncOperation<Integer> result= mwBoard.readRssi();
        result.onComplete(new AsyncOperation.CompletionHandler<Integer>() {
            @Override
            public void success(final Integer result) {
                Log.i("MainActivity", String.format("Rssi (%d dBm)", result));
            }

            @Override
            public void failure(Throwable error) {
                Log.e("AsyncResult Example", "Error reading RSSI value", error);
            }
        });
    }

    // This method will be called when a MessageEvent is posted
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        System.out.println("changed" + event);
        stepsSinceLastCommit = Integer.toString(sharedPreferences.getInt("stepsSinceLastCommit", 0));
        stepCountTextView.setText(stepsSinceLastCommit);
        //Animate the step icon
        if (stepImageSwitch == 0) {
            stepImageView.setImageResource(R.drawable.step_big_2);
            stepImageSwitch++;
        } else if (stepImageSwitch == 1){
            stepImageView.setImageResource(R.drawable.step_big_3);
            stepImageSwitch++;
        } else if (stepImageSwitch == 2){
            stepImageView.setImageResource(R.drawable.step_big_2);
            stepImageSwitch++;
        } else {
            stepImageView.setImageResource(R.drawable.step_big);
            stepImageSwitch = 0;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unbind the service when the activity is destroyed
        getActivity().unbindService(this);
    }


    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        // Typecast the binder to the service's LocalBinder class
        MetaWearBleService.LocalBinder binder = (MetaWearBleService.LocalBinder) service;

        final BluetoothManager btManager= (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        final BluetoothDevice remoteDevice= btManager.getAdapter().getRemoteDevice(MW_MAC_ADDRESS);

        binder.executeOnUiThread();
        //binder.clearCachedState(remoteDevice);
        mwBoard= binder.getMetaWearBoard(remoteDevice);
        mwBoard.setConnectionStateHandler(new ConnectionStateHandler() {
            @Override
            public void connected() {
                Toast.makeText(getActivity(), "Connected to MetaWear temperature sensor", Toast.LENGTH_LONG).show();

                Log.i("test", "Connected");
                Log.i("test", "MetaBoot? " + mwBoard.inMetaBootMode());
                multitempMe(rootView);


                mwBoard.readDeviceInformation().onComplete(new CompletionHandler<MetaWearBoard.DeviceInformation>() {
                    @Override
                    public void success(MetaWearBoard.DeviceInformation result) {
                        Log.i("test", "Device Information: " + result.toString());
                    }

                    @Override
                    public void failure(Throwable error) {
                        Log.e("test", "Error reading device information", error);
                    }
                });

            }
        });
        if (mwBoard != null) {
            mwBoard.connect();

        }

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }


    // Updates the temperature view in case the sensor is available & Bluetooth is turned on
    private List<MultiChannelTemperature.Source> sources;
    private AsyncOperation<RouteManager> multiTemp, onDieTemp;
    private boolean multitempSetup= false;
    public void multitempMe(View v) {
        try {
            final MultiChannelTemperature multiTempModule = mwBoard.getModule(MultiChannelTemperature.class);

            if (!multitempSetup) {
                sources = multiTempModule.getSources();
                multiTemp = multiTempModule.routeData().fromSource(sources.get(1)).stream("multi_thermistor").commit();
                if (sources.get(1) instanceof MultiChannelTemperature.ExtThermistor) {
                    MultiChannelTemperature.ExtThermistor thermistor = (MultiChannelTemperature.ExtThermistor) sources.get(1);
                    thermistor.configure((byte) 0, (byte) 1, false);
                }

                onDieTemp = multiTempModule.routeData().fromSource(sources.get(0)).stream("multi_on_die").commit();
                multitempSetup = true;

                multiTemp.onComplete(new CompletionHandler<RouteManager>() {
                    @Override
                    public void success(RouteManager result) {
                        result.subscribe("multi_thermistor", new RouteManager.MessageHandler() {
                            @Override
                            public void process(Message msg) {
                                Log.i("test", String.format("Ext thermistor: %.3f", msg.getData(Float.class)));
                                currentTempTextView.setText(getString(R.string.currentTemp));
                                temperatureTextView.setText(msg.getData(Float.class).toString() + " " + getString(R.string.celsius));
                            }
                        });
                        multiTempModule.readTemperature(sources.get(0));
                    }
                });
                onDieTemp.onComplete(new CompletionHandler<RouteManager>() {
                    @Override
                    public void success(RouteManager result) {
                        result.subscribe("multi_on_die", new RouteManager.MessageHandler() {
                            @Override
                            public void process(Message msg) {
                                Log.i("test", String.format("on-die temperature: %.3f", msg.getData(Float.class)));
                            }
                        });
                        multiTempModule.readTemperature(sources.get(1));
                    }
                });

                multitempSetup = true;
            } else {
                multiTemp.onComplete(new CompletionHandler<RouteManager>() {
                    @Override
                    public void success(RouteManager result) {
                        multiTempModule.readTemperature(sources.get(0));
                    }
                });
                onDieTemp.onComplete(new CompletionHandler<RouteManager>() {
                    @Override
                    public void success(RouteManager result) {
                        multiTempModule.readTemperature(sources.get(1));
                    }
                });
            }
        } catch (UnsupportedModuleException e) {
            Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }
}

